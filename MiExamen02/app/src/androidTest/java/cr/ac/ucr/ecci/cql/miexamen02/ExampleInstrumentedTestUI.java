package cr.ac.ucr.ecci.cql.miexamen02;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;

import static org.hamcrest.Matchers.anything;



import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTestUI {


    public static final String TEST_STRING_ID = "1001";
    public static final String TEST_STRING_NAME = "Immanuel Kant";

    @Rule
    public ActivityTestRule mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void listView_isCorrect() {
        // check list view is visible
        onView(withId(R.id.list)).check(matches(isDisplayed()));
    }


    @Test
    public void testId() {
        onData(anything()).inAdapterView(withId(R.id.list)).atPosition(0).perform(click());
        onView(withId(R.id.ID)).check(matches(withText(TEST_STRING_ID)));
    }

    @Test
    public void testName() {
        onData(anything()).inAdapterView(withId(R.id.list)).atPosition(0).perform(click());
        onView(withId(R.id.nombrePers)).check(matches(withText(TEST_STRING_NAME)));
    }



}

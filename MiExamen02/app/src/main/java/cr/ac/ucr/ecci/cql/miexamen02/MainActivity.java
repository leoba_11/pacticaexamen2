package cr.ac.ucr.ecci.cql.miexamen02;

import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen02.interfaces.MainActivityPresenter;
import cr.ac.ucr.ecci.cql.miexamen02.interfaces.MainActivityView;


public class MainActivity extends AppCompatActivity implements MainActivityView, AdapterView.OnItemClickListener{

    private ListView mListView;
    private MainActivityPresenter mMainActivityPresenter;
    private static Context myContext;

    List<Persona> personas = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListView = findViewById(R.id.list);
        mListView.setOnItemClickListener(this);

        // Llamada al Presenter
        mMainActivityPresenter =  new MainActivityPresenterImp(this);
        myContext = getApplicationContext();
    }


    // Mostrar los items de la lista en la UI con la lista de items muestra la lista
    @Override
    public void setItems(List<Persona> items) {
        LazyAdapter mAdapter = new LazyAdapter(items, this);
        mListView.setAdapter(mAdapter);
    }


    // Evento al dar clic en la lista
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Persona item = (Persona) mListView.getAdapter().getItem(position);

        Intent detailActivity = new Intent(MainActivity.this, detallesActivity.class);
        detailActivity.putExtra("persona", item);
        startActivity(detailActivity);
    }

    @Override
    protected void onDestroy() {
        mMainActivityPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMainActivityPresenter.onResume();
    }

}
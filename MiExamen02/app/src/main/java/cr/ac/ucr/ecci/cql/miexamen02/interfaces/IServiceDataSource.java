package cr.ac.ucr.ecci.cql.miexamen02.interfaces;

import android.content.Context;

import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen02.CantRetrieveItemsException;
import cr.ac.ucr.ecci.cql.miexamen02.Persona;

public interface IServiceDataSource {

    List<Persona> obtainItems () throws CantRetrieveItemsException;

}

package cr.ac.ucr.ecci.cql.miexamen02;


import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen02.interfaces.IServiceDataSource;
import cr.ac.ucr.ecci.cql.miexamen02.interfaces.PersonasInteractor;

public class PersonasInteractorImp implements PersonasInteractor {

    private IServiceDataSource service;

    @Override
    public void getItems(final PersonasInteractor.OnFinishedListener listener) {


        List<Persona> personas = null;
        service = new IServiceDataSourceImp();

        try {
            // obtenemos los items
            personas = service.obtainItems();
        } catch (CantRetrieveItemsException e) {
            e.printStackTrace();
        }

        // Al finalizar retornamos los items
        listener.onFinished(personas);
    }


}

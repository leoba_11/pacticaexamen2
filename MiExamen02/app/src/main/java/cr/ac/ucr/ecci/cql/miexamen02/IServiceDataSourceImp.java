package cr.ac.ucr.ecci.cql.miexamen02;

import android.content.Context;
import com.google.gson.Gson;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen02.interfaces.IServiceDataSource;

public class IServiceDataSourceImp implements IServiceDataSource {

    public static final String TAG_IMG = "IMG";

    //URL correspondiente a N=4
    String urlJson = "https://bitbucket.org/lyonv/ci0161_i2020_exii/raw/4250ce77dc7fdb5f5c1324d8c8d5bf5ff3959740/Personas4.json";

    @Override
    public List<Persona> obtainItems() throws CantRetrieveItemsException {
        List<Persona> personas = null;

        try {
            TaskServicioREST taskRest = new TaskServicioREST();
            taskRest.execute(urlJson);
            String data = getSplitJson( taskRest.get() );
            Gson gson = new Gson();
            Persona[] jsonObject = gson.fromJson(data, Persona[].class);
            personas = new ArrayList<>(Arrays.asList(jsonObject));

        }catch (Exception e) {
            throw new CantRetrieveItemsException(e.getMessage());
        }

        //this.print(personas);
        return personas;
    }


    // Para obterner solo la parte del json que ocupo
    public String getSplitJson(String json) {
        int inicio = json.indexOf("[");
        int fin = json.length() - 1;

        return json.substring(inicio, fin);
    }

    //imprimir nombres de personas
    public void print(List<Persona> lista) {
        for (int i = 0; i < lista.size(); i++){
            System.out.println(lista.get(i).getNombre());
        }
    }


    private class TaskServicioREST extends AsyncTask<String, Void, String> {

        // La tarea se ejecuta en un thread tomando como parametro el eviado en ** AsyncTask.execute() **
        @Override
        protected String doInBackground(String... urls) {
            // tomanos el parámetro del execute() y bajamos el contenido
            return loadContentFromNetwork(urls[0]);
        }


        // metodo para bajar el contenido
        private String loadContentFromNetwork(String url) {
            try {
                InputStream mInputStream = (InputStream) new URL(url).getContent();
                InputStreamReader mInputStreamReader = new InputStreamReader(mInputStream);
                BufferedReader responseBuffer = new BufferedReader(mInputStreamReader);
                StringBuilder strBuilder = new StringBuilder();
                String line = null;
                while ((line = responseBuffer.readLine()) != null) {
                    strBuilder.append(line);
                }

                return strBuilder.toString();
            } catch (Exception e) {
                Log.v(TAG_IMG, e.getMessage());
            }
            return null;
        }
    }


}

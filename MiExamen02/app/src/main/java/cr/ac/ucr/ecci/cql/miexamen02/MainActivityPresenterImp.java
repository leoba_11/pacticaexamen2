package cr.ac.ucr.ecci.cql.miexamen02;

import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen02.interfaces.MainActivityPresenter;
import cr.ac.ucr.ecci.cql.miexamen02.interfaces.MainActivityView;
import cr.ac.ucr.ecci.cql.miexamen02.interfaces.PersonasInteractor;

public class MainActivityPresenterImp implements
        MainActivityPresenter,
        PersonasInteractor.OnFinishedListener {

    private MainActivityView mMainActivityView;
    private PersonasInteractor mPersonasInteractor;

    public MainActivityPresenterImp(MainActivityView mainActivityView) {
        this.mMainActivityView = mainActivityView;

        // Capa de negocios (Interactor)
        this.mPersonasInteractor = new PersonasInteractorImp();
    }

    @Override
    public void onResume() {

        // Obtener los items de la capa de negocios (Interactor)
        mPersonasInteractor.getItems(this);
    }

    @Override
    public void onDestroy() {
        mMainActivityView = null;

    }

    @Override
    public void onFinished(List<Persona> items) {
        if (mMainActivityView != null) {
            mMainActivityView.setItems(items);
        }
    }

    // Retornar la vista
    public MainActivityView getMainView() {
        return mMainActivityView;
    }

}

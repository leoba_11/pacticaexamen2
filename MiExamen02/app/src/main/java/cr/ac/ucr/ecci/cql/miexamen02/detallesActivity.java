package cr.ac.ucr.ecci.cql.miexamen02;

import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentTransaction;
import android.os.Bundle;

public class detallesActivity extends AppCompatActivity {

    Persona persona = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles);
        persona = getIntent().getParcelableExtra("persona");
        setFragment(persona);
    }

    public void setFragment(Persona persona) {

        Bundle bundle = new Bundle();
        PersonaFragment detalles = new PersonaFragment();
        bundle.putParcelable("currentItem", persona);
        detalles.setArguments(bundle);
        FragmentTransaction fragTrac = getFragmentManager().beginTransaction();
        fragTrac.replace(R.id.persona_detalles, detalles);
        fragTrac.commit();
    }
}
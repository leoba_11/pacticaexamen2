package cr.ac.ucr.ecci.cql.miexamen02.interfaces;

import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen02.Persona;

public interface MainActivityView {

    // Mostrar los items de la lista en la UI
    void setItems(List<Persona> items);

}

package cr.ac.ucr.ecci.cql.miexamen02.interfaces;

import cr.ac.ucr.ecci.cql.miexamen02.Persona;

public interface MainActivityPresenter {

    // resumir
    void onResume();

    // destruir
    void onDestroy();


}

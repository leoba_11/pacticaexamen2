package cr.ac.ucr.ecci.cql.miexamen02.interfaces;

import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen02.Persona;

public interface PersonasInteractor {

    interface OnFinishedListener {
        void onFinished(List<Persona> items);
    }

    void getItems(OnFinishedListener listener);

}
